# LudicApp

Questo progetto effettuato con il fondo Bando alle ciance propone un esempio di Social Network a costo zero, fatto con react per la parte frontend, firebase per la parte backend utilizzando google cloud per la parte di hosting

## Comandi utili
Per eseguire il progetto lanciare nella cartella del programma 
```
npm start
```
Per aggiornare il sito firebase di hosting
```
npm run build;
firebase deploy
```
## Link utili
- [Link a guida Firebase Hosting](https://medium.com/swlh/how-to-deploy-a-react-app-with-firebase-hosting-98063c5bf425)
- [Link a guida creazione sito React + Firebase](https://www.youtube.com/watch?v=zL0dKETbCNE&list=PLDHWK63BVywzc_uN3RnQq-z_dg7VJbokv&index=2&t=3165s)

## Note sull'interfaccia grafica
### Colori
Green background #25465f
Dark green #2f455e
Yellow #eecd00

## Chi siamo
Sviluppo software a cura di Carlo Tacchella.

