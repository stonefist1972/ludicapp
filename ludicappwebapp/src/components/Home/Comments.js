import React, { useEffect, useState } from "react";
import { createDocInCollection, deleteDocInCollection, changeDocInCollection, getComments } from "../../db/db-mng";

function Comments(props) {

    const [posts, setPosts] = useState([]);
    const [myComment, setMyComment] = useState(null);
    const [text, setText] = useState("");
    const [titleText, setTitleText] = useState("");


    function padTo2Digits(num) {
        return num.toString().padStart(2, '0');
    }

    function formatDate(date) {
        return [
            padTo2Digits(date.getDate()),
            padTo2Digits(date.getMonth() + 1),
            date.getFullYear(),
        ].join('/');
    }
    const createComment = async () => {

        if (text === "" || titleText === "") {
            alert("Immetti tutti i campi in maniera corretta");
        }
        else if (props.userId !== undefined && props.userId !== null) {

            const time = new Date();

            let payload = {
                text: text,
                titleText: titleText,
                author: props.userName,
                date: formatDate(time),
                uid: props.userId,

            };

            
            await createDocInCollection(
                payload,
                "comments/"+props.id+"/feed"
            );
            
            setMyComment(payload);
            getPosts();

        }

    }

    const deleteComment = async (idComment) => {

        
        const confirmBox = window.confirm(
            "Vuoi Davvero eliminare il commento?"
        )
        if (confirmBox === true) {
            if (props.userId !== undefined && props.userId !== null) {
                await deleteDocInCollection(
                    idComment,
                    "comments/"+props.id+"/feed"
                );            
                setMyComment(null);
                getPosts();
            }
        }
    }
    
    const updateComment = async (idComment) => {

        if (props.userId !== undefined && props.userId !== null) {


            const time = new Date();
            let payload = {
                text: text,
                titleText: titleText,
                author: props.userName,
                date: formatDate(time),
                uid: props.userId,

            };

            await changeDocInCollection(
                payload,
                "comments/"+props.id+"/feed/",
                idComment
            );
            
            setMyComment(null);
            getPosts();

        }
    }
    

    const getPosts = async () => {
        const data = await getComments(props.id);
        const temp = data.docs.map((doc) => ({ ...doc.data(), id: doc.id }))
        setPosts(temp);
        getMyComment(temp);
    };

    const getMyComment = (temp) => {
        for (var p of temp) {
            if (p.uid === props.userId) {
                setMyComment(p);
                break;
            }
        }

    };

    useEffect(() => {
        getPosts();
    },[props]);

    //if i am not the author of the post and i am logged 
    return <div>

        {(props.userId !== props.authorId && props.userId !== null && props.userId !== undefined &&
            <div>

{(myComment === null &&
                <div className="article-preview">
                    <p>Lascia un commento sulla tua esperienza</p>
                    <fieldset className="form-group">
                        <input
                            className="form-control"
                            type="text"
                            placeholder="Titolo del commento"
                            onChange={
                                (event) => {
                                    setTitleText(event.target.value);
                                }
                            }
                        />

                    </fieldset>

                    
                    <fieldset className="form-group">
                    <textarea
                      rows={5}
                      className="form-control"
                      type="text"
                      placeholder="Descrizione dell'esperienza"
                      onChange={
                          (event) => {
                            setText(event.target.value);
                          }
                      }
                    />
                    </fieldset>

                    <button
                        className="btn btn-primary"
                        onClick={createComment}
                        type="submit">
                        Aggiungi
                    </button>
                </div>
)}
{(myComment !== null &&
                <div className="article-preview">

                    <p>Il tuo commento:</p>

                    <fieldset className="form-group">
                        <input
                            className="form-control"
                            type="text"
                            placeholder={myComment.titleText}
                            onChange={
                                (event) => {
                                    setTitleText(event.target.value);
                                }
                            }
                        />

                    </fieldset>
                    
                    
                    <fieldset className="form-group">
                    <textarea
                      rows={5}
                      className="form-control"
                      type="text"
                      placeholder={myComment.text}
                      onChange={
                          (event) => {
                            setText(event.target.value);
                          }
                      }
                    />
                    </fieldset>

                    <button
                        className="btn btn-danger"
                        onClick={()=>{deleteComment(myComment.id)}}
                        type="submit">
                        Elimina
                    </button>

                    <button
                        className="btn btn-primary pull-xs-right"
                        onClick={()=>{updateComment(myComment.id)}}
                        type="submit">
                        Modifica
                    </button>

                </div>
)}
            </div>)
        }
        {posts.map((post) => {
            return (
                <div key={post.id}>
                    <div className="article-preview" >
                        <div className="article-meta">
                            <div className="info">
                                <div className="author" >
                                    {post.author}
                                </div>
                                <span className="date">
                                    {post.date}
                                </span>
                            </div>
                        </div>
                        <div className="preview-link">
                            <h1>{post.titleText}</h1>
                            <h6>{post.text}</h6>
                        </div>
                    </div>
                </div>
            );
        })}
    </div>
}
export default Comments;

/*
                    <fieldset className="form-group">
                        <input
                            className="form-control"
                            type="text"
                            placeholder={myComment.titleText}
                            onChange={
                                (event) => {
                                    setTitleText(event.target.value);
                                }
                            }
                        />

                    </fieldset>
                    <fieldset className="form-group">
                        <input
                            className="form-control"
                            type="text"
                            placeholder={myComment.text}
                            onChange={
                                (event) => {
                                    setText(event.target.value);
                                }
                            }
                        />

                    </fieldset>

*/