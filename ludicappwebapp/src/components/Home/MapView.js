import React, { useState } from 'react';
import { getLocation } from '../../geo/geo-mng';
import { useNavigate } from "react-router-dom";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import * as L from "leaflet";
import colorGame from '../../assets/img/m_blue.png';
import colorEvent from '../../assets/img/m_red.png';
import colorAssociation from '../../assets/img/m_orange.png';
import colorShop from '../../assets/img/m_green.png';

const MapView = (props) => {

  let navigate = useNavigate();

  const LeafIcon = L.Icon.extend({
    options: {}
  });

  const colorGameIcon = new LeafIcon({
    iconUrl: require('../../assets/img/m_blue.png'),
    iconSize: new L.Point(25, 41),
  }),
    colorEventIcon = new LeafIcon({
      iconUrl: require('../../assets/img/m_red.png'),
      iconSize: new L.Point(25, 41),
    }),
    colorAssociationIcon = new LeafIcon({
      iconUrl: require('../../assets/img/m_orange.png'),
      iconSize: new L.Point(25, 41),
    }),
    colorShopIcon = new LeafIcon({
      iconUrl: require('../../assets/img/m_green.png'),
      iconSize: new L.Point(25, 41),
    });

  const [initPos, setInitPos] = useState(
    {
      lat: "45.4388703",
      long: "10.9884333"
    }
  );
  const zoomVal = 10;
  const [map, setMap] = useState(null);

  const flyMeTo = (par) => {
    setInitPos({ lat: par.lat, long: par.long });
    map.flyTo([par.lat, par.long], zoomVal);
  }

  const changePos = async () => {
    let loc = await getLocation();
    flyMeTo(loc);
  }
  const goToGuide = async () => {
    //go to guide page
    navigate("/guide");
  }
  const getColor = (type) => {
    if (type === "game") {
      return colorGameIcon;
    }
    else if (type === "association") {
      return colorAssociationIcon;
    }
    else if (type === "shop") {
      return colorShopIcon;
    }
    else {
      return colorEventIcon;
    }
  }
  return (
    <div>
    <div className="banner-map">

      <MapContainer
        center={[initPos.lat, initPos.long]}
        zoom={zoomVal}
        whenCreated={setMap}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {
          props.data.map((item, index) => (
            <Marker
              icon={getColor(item.type)}
              key={index}
              position={[
                item.loc[0],
                item.loc[1]
              ]}
            >
              <Popup>

                <b>{item.title.toUpperCase()}</b>
                <button
                  className="btn btn-sm btn-primary"
                  style={{
                    margin: 'auto',
                    marginTop: 10,
                    display: 'block'
                  }}
                  onClick={() => {
                    props.callback(index);
                  }}
                >INFO</button>
              </Popup>
            </Marker>
          ))
        }

      </MapContainer>
<div>
      <button
        className="btn btn-sm pull-xs-left btn-primary"
        onClick={() => {
          goToGuide();
        }}
      >Regolamento</button>

      <button
        className="btn btn-sm pull-xs-right btn-primary"
        onClick={() => {
          changePos();
        }}
      >La mia posizione</button>
      </div>
    </div>

    <div  className="banner-map">
      <h5>
        Benvenuto in <b>LudicApp</b>!
      </h5>
      <h5>
      La Web App nata per unire persone che vogliano giocare insieme a giochi in scatola oppure scambiarli per brevi periodi di tempo.
      </h5>
        <h5>

        <img src={colorGame} style={{height: 40, marginRight:10}}></img>
              
              Mi trovi qui per giocare e/o prestare
              </h5>
        <h5>
        <img src={colorEvent} style={{height: 40, marginRight:10}}></img>
              Evento a tema di giochi da tavolo
              </h5>
        <h5>
        <img src={colorAssociation} style={{height: 40, marginRight:10}}></img>
              Associazione
        </h5>
        <h5>
        <img src={colorShop} style={{height: 40, marginRight:10}}></img>
              Negozio
        </h5>
      </div>
    </div>
  );
};

export default MapView;
