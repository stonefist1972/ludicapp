import React from "react";

import YoutubeEmbed from "../../Team/YoutubeEmbed";
import Banner from '../../Team/Banner';

import { FaFacebook, FaInstagram } from 'react-icons/fa';
function Team() {
    return <div>
        <Banner></Banner>


        <div className="container page">
            <div className="row article-content">
                <div className="col-xs-12">

                    <h4><b>Il nostro Team:</b></h4>
                    <YoutubeEmbed embedId="1yGmkzMPRcs" />

                    <div className="banner">
                        <div className="container">
                            <p>
                                Siamo un gruppo di ragazzƏ che hanno in comune la passione dei giochi da tavolo. Alcun3 giocano in associazioni, altrƏ solo casualmente con amicƏ. Unendo i nostri bisogni di giocatorƏ abbiamo pensato di trovarci per realizzare qualcosa che possa rispondere alle nostre difficoltà nell'incontrare persone nuove con cui giocare e rinnovare il materiale ludico con cui divertirci.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <br></br>
            <div className="row article-content">
                <div className="col-xs-12">
                    <h4><b>I nostri Sponsor:</b></h4>
                    <YoutubeEmbed embedId="KSZ6ncrrjs4" />

                    <div className="banner">
                        <div className="container">
                            <p>
                                Bando alle Ciance 2021 è un’iniziativa per lo sviluppo di progetti ed idee promosse “dai giovani per i giovani”, cofinanziato dalla Presidenza del Consiglio dei Ministri – Dipartimento per le Politiche Giovanili e il Servizio Civile Universale.
                                Costituiscono la rete progettuale il Comune di Sona (Capofila) e i 36 Comuni del Comitato dei Sindaci del Distretto Ovest Veronese dell’Azienda Ulss 9 Scaligera, l’Azienda Ulss 9 Scaligera, Fondazione Edulife e le Associazioni giovanili (E)Vento tra i Salici, Da Vicino Concerti e Liberamente Cavaion.

                                Il Bando alle Ciance valorizza la creatività, potenziando le competenze trasversali e lo spirito di iniziativa, offrendo strumenti e mezzi per progettare e realizzare attività sociali e culturali sul territorio.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row article-content">
                <div className="col-xs-12">

        <h5>
            Ci trovi su {' '}
            <a
                href="https://www.facebook.com/LUDICAPP.uniamogiocator3"
                target='_blank'
                rel="noopener noreferrer"
            >

                <b>Facebook</b> {' '}
              <FaFacebook />

            </a>{' '}
            e{' '}
            <a
                href="https://www.instagram.com/ludicapp_uniamogiocator3/"
                target='_blank'
                rel="noopener noreferrer"
            >
                <b>Instagram</b> {' '}
                <FaInstagram/>
            </a>
        </h5>

        <h5>
            Email: <b>info.ludicapp@gmail.com</b>
        </h5>

        <h5>
            Cellulare: <b>+39 329 206 4129</b>
        </h5>
                </div>
            </div>
        </div>
    </div>;
}

export default Team;