import React, { useEffect, useState } from "react";
import { auth } from "../../../db/firebase-config";
import { useNavigate } from "react-router-dom";
import { createDocPosts } from "../../../db/db-mng";
import { fromAddressToLatLong } from "../../../geo/geo-mng";

function CreatePost({ isAuth }) {

  const [type, setType] = useState("");
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [postPosLat, setPostPosLat] = useState("");
  const [postPosLong, setPostPosLong] = useState("");
  const [address, setAddress] = useState("");
  const [gameList, setGameList] = useState("");
  const [eventData, setEventData] = useState("");
  const [nameComplete, setNameComplete] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [addressFound, setAddressFound] = useState("");

  let navigate = useNavigate();

  const getPayload = () => {

    const date = new Date();

    if (type === "event") {
      if (eventData === "")
        return null;
    }

    if (title === "" ||
      type === "" ||
      address === "" ||
      (email === "" &&
      phone === "") ||
      postPosLat === "" ||
      postPosLong === ""
    ) {
      return null;
    }
    return {
      title: title,
      description: description,
      type: type,
      address: address,
      date: date,
      nameComplete: nameComplete,
      email: email,
      phone: phone,
      author:
      {
        name: auth.currentUser.displayName,
        id_user: auth.currentUser.uid,
        email: auth.currentUser.email,
      },
      loc: [
        postPosLat,
        postPosLong
      ],
      event: {
        eventData: eventData
      },
      game: {
        gameList: gameList
      },
    };

  }

  const create = async () => {
    let payload = getPayload();
    if (payload == null) {
      alert("Inserisci almeno il nome che compare sulla mappa, l'indirizzo, le coordinate e un contatto (email oppure telefono)")
    }
    else {
      await createDocPosts(
        payload
      );
      navigate("/");
    }
  };

  const getLatLong = async () => {
    if (address !== "") {
      fromAddressToLatLong(address).then(
        (res) => {
          setPostPosLat(res.lat);
          setPostPosLong(res.long);
          setAddressFound(res.addressFound);
        }
      ).catch((err) => {
        alert("Indirizzo non trovato, prova a essere più preciso o inserisci manualmente le coordinate.")
      });
    }
    else {
      alert("Inserisci un indirizzo");
    }
  }

  useEffect(() => {

    if (!isAuth) {
      navigate("/");
    }
  });

  const onFirst = () => {
    setType('game');
  }

  const onSecond = () => {
    setType('event');
  }

  const onThird = () => {
    setType('association');
  }

  const onFourth = () => {
    setType('shop');
  }

  return <div className="createPostPage">

    <div className="normal-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-10 offset-md-1 col-xs-12">


            <div className="banner">
              <div className="container">
                <p>Crea il tuo scambio o il tuo evento che comparirà sulla mappa!</p>

                <fieldset>
                  <p
                  >Cosa vuoi registrare?</p>

                  <input type="radio" id="game" name="type" onClick={onFirst} />
                  <label className="type-label"><b>Mi trovi qui <br/>per giocare e/o prestare</b></label><br />

                  <input type="radio" id="event" name="type" onClick={onSecond} />
                  <label className="type-label"><b>Evento a tema di GdT</b></label><br />

                  <input type="radio" id="association" name="type" onClick={onThird} />
                  <label className="type-label"><b>Associazione</b></label><br />

                  <input type="radio" id="shop" name="type" onClick={onFourth} />
                  <label className="type-label"><b>Negozio</b></label>

                </fieldset>
              </div>
            </div>

            {(type === "game" || type === "event" || type === "association" || type === "shop") && (


              <div>
                <fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Nome che compare sulla mappa"
                      onChange={
                        (event) => {
                          setTitle(event.target.value);
                        }
                      }
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <textarea
                     rows={5}
                      className="form-control"
                      type="text"
                      placeholder="Descrizione e note aggiuntive"
                      onChange={
                        (event) => {
                          setDescription(event.target.value);
                        }
                      }
                    />
                  </fieldset>

                  {type === "game" && (
                    <fieldset className="form-group">
                      <p>Scrivi una lista di giochi che vuoi mettere a disposizione</p>

                      <input
                        className="form-control"
                        type="text"
                        placeholder="Lista dei giochi"
                        onChange={
                          (event) => {
                            setGameList(event.target.value);
                          }
                        }
                      />
                    </fieldset>
                  )}
                  {type === "event" && (
                    <fieldset className="form-group">
                      <p>Scrivi la data dell'evento o ogni quanto si ripete</p>

                      <input
                        className="form-control"
                        type="text"
                        placeholder="Date dell'evento"
                        onChange={
                          (event) => {
                            setEventData(event.target.value);
                          }
                        }
                      />
                    </fieldset>
                  )}


                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Indirizzo del luogo"
                      onChange={
                        (event) => {
                          setAddress(event.target.value);
                        }
                      }
                    />

                    <button
                      className="btn btn-primary pull-xs-right"
                      onClick={getLatLong}
                      type="submit">
                      Calcola coordinate
                    </button>

                  </fieldset>

                  {addressFound !== "" && (<p>{addressFound}</p>)}

                  <fieldset className="form-group">
                    <input

                      className="form-control-half form-control-half-left"

                      type="text"
                      onChange={
                        (event) => {
                          setPostPosLat(event.target.value);
                        }
                      }
                      placeholder={postPosLat === "" ? "Latitudine" : postPosLat}
                    />

                    <input

                      className="form-control-half form-control-half-right"
                      type="text"
                      onChange={
                        (event) => {
                          setPostPosLong(event.target.value);
                        }
                      }
                      placeholder={postPosLong === "" ? "Longitudine" : postPosLong}
                    />

                    <fieldset className="form-group">
                      <input
                        className="form-control"
                        type="text"
                        placeholder="Nome completo autore del post"
                        onChange={
                          (event) => {
                            setNameComplete(event.target.value);
                          }
                        }
                      />
                    </fieldset>

                  </fieldset>
                  <p>Indirizzo email</p>
                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Email"
                      onChange={
                        (event) => {
                          setEmail(event.target.value);
                        }
                      }
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control"
                      type="text"
                      placeholder="Numero di telefono (opzionale)"
                      onChange={
                        (event) => {
                          setPhone(event.target.value);
                        }
                      }
                    />
                  </fieldset>

                  <button

                    className="btn btn-primary pull-xs-right"
                    type="button"
                    onClick={create}>
                    Crea
                  </button>

                </fieldset>
              </div>

            )}

          </div>

        </div>
      </div>
    </div>
  </div>
}
export default CreatePost;