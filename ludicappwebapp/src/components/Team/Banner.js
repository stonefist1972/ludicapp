import React from 'react';

//import { FaFacebook, FaInstagram } from 'react-icons/fa';
const Banner = () => {
  return (
    <div className="banner">
      <div className="container">
       <h2 className="logo-font">
       <b>LUDICAPP</b>
        </h2>
        <h4>Uniamo giocator3</h4>
        <p>Questo strumento è nato per far incontrare giocator3.</p>
        <p>Quindi l’aspetto importante è geolocalizzarsi per far capire alle persone che: siete dei giocator3 e in che zona abitate; che avete la voglia di incontrare giocator3 come voi, trovare associazioni, negozi o eventi; e solo come opzione extra, potete mettere a disposizione dei giochi in prestito (qui sotto troverete il regolamento a riguardo).</p>
     </div>
    </div>
  );
};

export default Banner;