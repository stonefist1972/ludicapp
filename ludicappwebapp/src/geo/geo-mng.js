
  const getLocation = async() => {
    
        const pos = await new Promise((resolve, reject) => {
          if (!navigator.geolocation) {
            console.error('Geolocation is not supported by your browser');
          }
          else{
            navigator.geolocation.getCurrentPosition(resolve, reject);
          }
        });
    
        return {
          long: pos.coords.longitude,
          lat: pos.coords.latitude,
        };
  }

    // https://developer.mapquest.com/documentation/geocoding-api/address/get
    // http://www.mapquestapi.com/geocoding/v1/address?key=vCIgc7LamJd0qM7XnGCDK0SOT7HXwArb&location=Via%20Roma%2020%20Grezzana%20Verona
    const fromAddressToLatLong = async(par) => {
        
        
        let resp = await fetch('https://www.mapquestapi.com/geocoding/v1/address?key=vCIgc7LamJd0qM7XnGCDK0SOT7HXwArb&location='+par+' Italy')
            .then(response => response.json())
            .then(data => 
            {      
              let res =  data.results[0].locations[0];
               return {
                    lat: res.latLng.lat,
                    long: res.latLng.lng,
                    addressFound: "Le coordinate per "+res.street+" "
                    +res.adminArea5+" "
                    +res.adminArea4+" "
                    +res.adminArea3+" ("
                    +res.adminArea1+") - L'indirizzo è corretto? Riprova oppure immetti le coordinate manualmente! "
                };
            }
        );
        return resp;
  
  
    }

    export {
        fromAddressToLatLong,
        getLocation
    };
