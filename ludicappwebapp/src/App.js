import './App.css';
import {BrowserRouter, Routes, Route, Link} from "react-router-dom";
import { FaHome } from 'react-icons/fa';
import Home from './components/pages/Home/Home';
import Team from './components/pages/Team/Team';
import Guide from './components/pages/Guide/Guide';
import {useState} from "react";
import {auth, provider} from "./db/firebase-config";
import {signInWithPopup} from 'firebase/auth';
import {signOut} from "firebase/auth";
import CreatePost from './components/pages/CreatePost/CreatePost';
import icon from './assets/img/icon.png';
import packageJson from '../package.json';
import CookieConsent, { Cookies } from "react-cookie-consent";
import { SuperSEO } from "react-super-seo";

var DEBUG = false;
function App() {

  const [isAuth, setIsAuth] = useState(localStorage.getItem("isAuth"));

  const signInWithGoogle = () => {
      signInWithPopup(auth, provider).then((result)=>{
        localStorage.setItem("isAuth", true);
        setIsAuth(true);
      });
  }

  const signOutWithGoogle = () => {
      signOut(auth).then(()=>{
        localStorage.clear();
        setIsAuth(false);
      })
  }

  return (

    <BrowserRouter>
    
    <nav className="navbar navbar-light">
      <div className="container">

        <Link to="/" className="navbar-brand" 
        
  >
    <div >

        <img src={icon} style={{height: 40, marginRight:10}}
         alt = ""/>
        <b>LudicApp</b>

        </div>
        </Link>

        <ul className="nav navbar-nav pull-xs-right">
            <li className="nav-item">
              <Link to="/ourteam" className="nav-link">
                Chi siamo
              </Link>
            </li>
        {!isAuth && 
            <li className="nav-item">
              <Link to="/" onClick={signInWithGoogle} className="nav-link">
                Login 
              </Link> 
            </li>
        }


        {(isAuth || DEBUG) &&
            <li className="nav-item">
              <Link to="/post" className="nav-link">Crea</Link>
            </li>
        }
        {isAuth &&
            <li className="nav-item">
              <Link to="/" onClick = {signOutWithGoogle} className="nav-link">
                Logout </Link>
            </li>
        }

            <li className="nav-item">
              <Link to="/" className="nav-link">
              <FaHome />
              </Link>
            </li>
        </ul>
      </div>
    </nav>

      <Routes>
        <Route path="/" element={<Home isAuth={isAuth}/>}></Route>
        <Route path="/ourteam" element={<Team/>}></Route>
        <Route path="/post" element={<CreatePost isAuth={isAuth}/>}></Route>
        <Route path="/guide" element={<Guide/>}></Route>
      </Routes>

      <nav style = { {  position: 'absolute',  bottom:0, right:0}} className="navbar navbar-light">
      <p>V. {packageJson.version}</p>
      </nav>
      <CookieConsent buttonText="OK" >Questo sito utilizza cookies per migliorare la tua esperienza utente.</CookieConsent>
      <SuperSEO
        title="Ludicapp"
        description="Benvenuto in LudicApp!
        La Web App nata per unire persone che vogliano giocare insieme a giochi in scatola oppure scambiarli per brevi periodi di tempo."
      >
      </SuperSEO>  
    </BrowserRouter>
  );
}

export default App;
