import { initializeApp } from "firebase/app";
import {getFirestore} from "firebase/firestore";
import {getAuth, GoogleAuthProvider} from 'firebase/auth';
import { CONFIGURATION } from "../constants/configuration";
// https://firebase.google.com/docs/web/setup#available-libraries

const firebaseConfig = {

  apiKey: CONFIGURATION.firebaseConfig.apiKey,

  authDomain:  CONFIGURATION.firebaseConfig.authDomain,

  projectId: CONFIGURATION.firebaseConfig.projectId,

  storageBucket: CONFIGURATION.firebaseConfig.storageBucket,

  messagingSenderId: CONFIGURATION.firebaseConfig.messagingSenderId,

  appId: CONFIGURATION.firebaseConfig.appId

};


// Initialize Firebase

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const provider = new GoogleAuthProvider();
export const db =  getFirestore(app);